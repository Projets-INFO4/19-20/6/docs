# Projet - INFO 4 - S8

# Testeur radio LoRaWAN pour la platforme CampusIoT

## Tom Graugnard 
- Identifiant: graugnat
- Mail: tom.graugnard@etu.univ-grenoble-alpes.fr

## Morgan Crociati
- Identifiant: crociatm
- Mail: morgan.crociati@etu.univ-grenoble-alpes.fr

## Semaine 1 (20/01 - 26/01)
Découverte des différents projets possibles.
Choix du projet "**Testeur radio LoRaWAN pour la platforme CampusIoT**".

Nous avons choisi ce projet car nous aimons le fait que grâce à lui nous pouvons toucher à plusieurs technologies.

## Semaine 2 (27/01 - 02/02)
Cours de présentation de la technologie LoRaWAN. Découverte de la platform CampusIoT.

Explications techniques du projet:

A l'aide d'une carte LoRaWAN (LoPy 4) et d'un téléphone android, on veut pouvoir mesurer la qualitée du réseau LoRaWAN en fonction des éméteurs.
La carte LoRaWAN doit pouvoir communiquer ses résultats par Bluetooth au téléphone.
L'application Android sert ensuite de base de donnée de tous les tests éffectués.

## Semaine 3 (03/02 - 09/02)
Explication détaillée de l'application demandée:
- Application Android native
- Communication avec l'émeteur LoRaWAN (LoPy 4) en Bluetooth (BLE) pour échanger des données sur la qualitée du réseau.
- Communication avec les serveurs de CampusIoT, pour envoyer des données de qualité des réseaux et en recevoir.
- Stockage de données sur les tests réseaux (date, position, antenne, qualité, ...)
- Possibilité d'affichage sur une carte intéractive (antenne positionnée, position des tests et leurs qualitées, ...)

Conseil pour démarrer de Didier Donsez, utiliser **AppInventor** pour essayer de communiquer en Bluetooth entre la carte et un portable Android.
D'après Didier Donsez si AppInventor n'est pas trop limitant et que cela nous plaît tout le projet peut être fait avec.

Emeteur fournis pour le projet à aller chercher au **FabLab MSTIC**.

Prise de rendez-vous pour aller chercher l'éméteur le mercredi 05/02

Récupération de l'éméteur LoPy 4 au FabLab.

Explication d'utilisation de l'éméteur:
- **Ne jamais débrancher l'antenne**  (sinon le port sur la carte grille).
- Formater la carte LoPy 4 avant utilisation.
- Mettre à jour la carte LoPy 4 si nécéssaire.
- Utiliser l'antenne interne pour le Bluetooth BLE.

## Semaine 4 (10/02 - 16/02)

Découverte d'AppInventor
- [Lien vers AppInventor](http://ai2.appinventor.mit.edu)
- [Tutoriels générale pour AppInventor](https://appinventor.mit.edu/explore/ai2/tutorials)
- [Installation d'une application sur un téléphone Android](http://appinventor.mit.edu/explore/ai2/setup)
- [Tutoriel de connexion en Bluetooth BLE avec AppInventor](http://iot.appinventor.mit.edu/assets/tutorials/MIT_App_Inventor_Basic_Connection.pdf)

AppInventor semblant assez limité pour réaliser une application comme nous voulons faire, nous l'utiliserons uniquement pour essayer de communiquer avec la carte LoPy 4.

Installation d'Android Studio pour la suite.

## Semaine 5 (17/02 - 23/02)
Suite de l'apprentissage de AppInventor.

Suivi du guide de communication en Bluetooth avec une carte LoPy 4 ([Lien vers le guide](https://docs.pycom.io/firmwareapi/pycom/network/bluetooth/)).

Objectif durant l'intéruption pédagogique: apprendre à manier Android Studio pour la suite grâce à OpenClassroom. [Lien vers le cours en ligne](https://openclassrooms.com/fr/courses/4517166-developpez-votre-premiere-application-android).

Objectif pour après l'intéruption pédagogique: programmer la carte LoPy 4 pour communiquer avec notre application AppInventor.

## Semaine 6 (24/02 - 01/03)
Intéruption pédagogique d'hiver.

## Semaine 7 (02/03 - 08/03)
Objectif du jour: créer et installer un programme sur la carte LoPy 4.

Premier problème: impossibilitée de détecter la carte sur nos deux ordinateurs personnels. Découverte que la plupart des solutions en ligne concernent Windows et non Linux à notre grande tristesse.

Après installation de drivers et après être passé en super utilisateur, nous pouvons détecter la carte sur un des ordinateurs mais pas intéragir. On spouçonne que l'Experimental Board 3.0 sur le quelle est la carte est le problème car il est sensé faire le lien entre l'ordinateur et la carte LoPy 4. Nous le mettons donc à jour à l'aide des outils fournis sur [le site](https://docs.pycom.io/pytrackpysense/installation/firmware/). Grâce à une entré sur StackOverflow, on a vu qu'il pouvait y avoir un problème sur l'Experimental Board 3.0 si on n'arrivait pas intéragir avec la carte, et donc qu'il était conseillé d'effacer sa mémoire avant de la mettre à jour.

Une fois que nous détections correctemment la carte LoPy 4 sur l'Experimental Board nous avons éffacé sa mémoire pour ne pas garder l'ancien programme écrit dedans. Puis, installation de PyMakr pour pouvoir coder sur la carte. PyMakr n'est pas capable de se connecter sur l'ordinateur où la carte est detectée. Nous avons détecté que le problème venait du fait que la carte n'autorisait pas la connexion avec PyMakr, pour le savoir nous sommes passé par l'outil screen (`sudo apt-get install screen`).

Après beaucoup de recherches et d'essais, nous avons trouvé sur la documentation de l'Experimental Board 3.0 qu'il était possible de faire un Safe Boot pour l'Experimental Board ainsi que le LoPy 4. Le Safe Boot a permis d'intéragir avec la carte.

Une fois la carte connectée nous avons pu y importer un programme en MicroPython. Le programme se trouve sur notre GitLab dans le dossier "tests".

Entrevue avec Didier Donsez pour lui faire part de nos inquiétudes sur la faisabilité et l'objectif du projet (entre rendre quelque chose ou apprendre beaucoup de nouvelles technologies). Il est décidé que pour l'application nous utiliseront Android Studio (sous Java et non Kotlin) pour nous permettre d'avancer plus vite.

Quand nous avons voulu se remettre au projet, l'ordinateur sur lequel la carte fonctionnait donnait une erreur quand on lançait Ubuntu. Nous avons donc du réinstaller Ubuntu pour le faire fonctionner, et nous avons tout réinstallé pour que la carte fonctionne. Puis, nous avons commencé à faire une programme pour utiliser le BLE.

(Didier Donsez nous a conseillé de bien détailler dans notre journal de bord les problèmes que nous avons rencontré)

## Semaine 8 (09/03 - 15/03)
Soutenances de mi-parcours.
Point important soulevé par la soutenance de mi-parcours:
- Le git est mal utilisé, on devrait faire plus de commit sur ce que l'on peut commit (journal de bord et code pour la carte LoPy 4).
- Journal de bord bien fait.
- Présentation à clarifier sur ce qu'est le réseau LoRaWAN (une antenne, un serveur ?).

Point soulevé avec Olivier Richard:
- Même si les outils utilisé ne nous plaise pas, c'est intéréssant pédagogiquement.
- Pour la soutenance finale, il pourrait être intéréssant de prendre du recule sur que l'on a fait en tant qu'ingénieur particulièrement sur AppInventor 2.
- Bien détailler notre parcours et le documenter.

Après avoir crée un programme pour activer la carte LoPy 4 en tant que serveur BLE, nous avons pu la détecter depuis notre téléphone. Mais impossible de s'associer avec la carte LoPy 4, après des recherches nous avons vue que le faites de s'associer n'est pas encore supporté (le problème a été soulevé sur github). En revanche nous avons aussi plus appris sur le fonctionnement du Bluetooth Low Energy, et nous avons découvert que en BLE il n'est pas nécéssaire de s'associer pour pouvoir communiquer.

AppInventor 2 ne supporte pas officiellement le BLE, nous avons donc du ajouter une extension à notre projet ([Lien vers le fichier extension ajoutant le BLE](http://iot.appinventor.mit.edu/assets/resources/edu.mit.appinventor.ble.aix)).

Une fois l'extension ajouté nous avons pu détecter notre LoPy 4 sur le réseaux depuis l'application et nous avons pu nous connecter (en ayant codé que la carte puisse accepter des connexions).

Nouveau problème rencontré, AppInventor ne parvient pas à envoyer des messages à la carte. L'identifiant de service BLE (uuid) et l'identifiant de charasteristique ne sont pas reconnue. En revanche on peut recevoir des messages si la carte nous en envoie, et possibilité de répondre à ses messages mais impossibilité d'envoyer un message spontané.

Objectif pour la prochaine séance:
- Pouvoir échangé des messages avec la carte LoPy 4.
- Se renseigner sur les échanges sur le réseaux LoRaWAN sur la carte.

## Semaine 9 (16/03 - 22/03) (Confinement)
Entretient avec Didier Donsez en vidéo conférence.

Problème rencontré suite au confinement:
- Une seul carte LoPy 4 (chez Morgan), donc impossibilité de travailler parfaitement en binôme sur le projet.
- Impossibilité pour Tom d'exporter et de tester du micropython avec les librairies Pycom pour avancer le projet.
- AppInventor ne favorise pas le travail en groupe séparé.
- Impossibilité de tester notre programme à différente localisation pour vérifié la qualitée des valeurs obtenues.

Solution pouvant peut être aider la suite du projet:
- Travailler beaucoup en appel vocal ET vidéo (aspect vidéo important pour pouvoir voir l'application ainsi que la carte lors de la réalisation de prototypes).
- Ajouter les fichiers propriétaire de AppInventor sur dépot Git pour pouvoir se les échangers (un peu plus éfficacement que par messagerie/mail/...).
- Trouver une solution pour partager le code en MicroPython pour pouvoir permettre de faire des testes de manières éfficace (codeshare.io peut être).

Découverte que la carte LoPy 4 encrypte (ou du moins modifie) l'uuid que nous lui entront lors de l'ouverture de notre serveur BLE. Solution envisageable envoyé le nouveau uuid à l'application et définir ce nouveau uuid comme celui par défaut (Nous allons surement ouvrir une issue ou un poste sur leur forum pour avoir plus d'information).

Solution trouvé sur le forum (la carte faisait une manipulation sur les octets donné comme uuid), création d'une librairie pour la carte pour pouvoir utiliser un uuid custom.

L'application peut envoyer des messages à la carte et la carte peut envoyer des messages à l'application.

## Semaine 10 (23/03 - 29/03) (Confinement)
Implémentation des communications en LoRaWAN, mais impossibilité de recevoir des réponses. On reçois des réponses mais que l'on arrive pas interpréter.
Il nous faudrait des vraies identifiants, je pense.

Entretient avec Dider Donsez par Skype:
- Utiliser le mode OPAA pour utiliser le réseau LoRa et pas ABP (qui était celui par défaut).
- [Lien du tutoriel](https://github.com/CampusIoT/tutorial/tree/master/loraserver) LoPy4 pour CampusIoT (pas les mêmes choses que notre projet mais on peut s'en inspirer)
- Il pourrait peut être bien de refaire le tutoriel LoPy4 de CampusIoT avec ce que l'on a appris.
- Création de notre compte pour CampusIoT d'ici la semaine prochaine.

Affichage d'une liste de réponses envoyé par la carte LoPy4 sur AppInventor (pour l'instant on affiche seulement des entiers).
Premier essaie de géolocalisation avec AppInventor 2.

## Semaine 11 (30/03 - 05/04) (Confinement)
Géolocalisation en temps réel réussie avec AppInventor.

Modification de la carte en temps réel lors de l'envoie d'une demande de d'envoie de paquet LoRa à notre LoPy4 (ajout d'une épingle sur la position et emplacement pour mettre les infos reçu par le serveur).

## Semaine 12 (06/04 - 12/04) (Confinement)
Report: Soutenance du projet et semaine d'examen.

Application des indications de Didier Donsez lors de la semain 10. C'est à dire première connexion en OTAA à CampusIoT.

Nous avons eu des gros problème de connexion à CampusIoT avec notre LoPy4. Car comme pour le BLE la carte fait des optimisations sur les adresses hexadecimal donné donc impossibilité d'arriver à faire se joindre les deux.

Solution trouvé écrire en ascii nos adresses en LoPy puis les encodé en l'inverse hexadécimal. Puis lors l'enregistrement sur CampusIoT mettre l'adresse en LSB et pas MSB.

De plus, la connexion est plutôt mauvaise de l'endroit où je me trouve pour tester la carte LoPy4, donc cela rendait difficile les tests pour savoir si la connexion avait fonctionné. J'avais au départ des temps de connexion au serveur de l'ordre de 10 minutes (avant que Didier Donsez nous conseille de nous rapprocher des fenêtres, voir même sortir la carte par la fenetre).

## Semaine 13 (13/04 - 19/04) (Confinement)
Envoie de paquet automatique vers CampusIoT de la carte. Comme on peut voir en temps-réel, nous avons pu voir le data-rate que nous avions au choisie au départ n'était pas mauvais pour notre position.

Commencement de la mise en place de MQTT sur AppInventor. La ressources donné par Didier Donsez n'est pas une extension d'AppInventor, il faudra donc faire le tout manuellement.

## Semaine 14 (20/04 - 26/04) (Confinement)
Recherche et essaie pour permettre la communication en MQTT. App Inventor ne possède aucune fonction permettant la communication sous MQTT et il ne permet pas de connexion TCP constante comme il est nécéssaire pour utiliser MQTT...

Première essaie, utiliser la connexion HTTP fournis par AppInventor pour passer par un relais (un serveur web local) nous envoyant les informations reçu du serveur CampusIoT. Après avoir recherché, cette solution me parrait trop difficile à mettre en place juste pour l'application même si les fonction d'AppInventor avec HTTP son simple d'utilisation.

Deuxième possibilitée trouvé, passer par le navigateur web intégré à AppInventor et y importer du code Javascript communicant manuellement en MQTT. Cette solution aurait été faisable avec plus de temps, je pense.

## Semaine 15 (27/04 - 30/04) (Confinement)
Rédaction du rapport
